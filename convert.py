#!python3

from pathlib import Path
import datetime
from spacepy import pycdf
from astropy.units import Unit, Quantity


DATA_PATH = Path(__file__).parent / "data"
FILE_TXT = DATA_PATH / f"PlanetaryFluxDensities.txt"
FILE_SAV = DATA_PATH / 'all_spectra.sav'
FILE_CDF = DATA_PATH / 'all_spectra.cdf'

CDF_REAL4_FILLVAL = -1.0e31
DEFAULT_DATA_VAR_ATTRS = {
    "UNITS": "W/m^2/Hz",
    "DEPEND_0": "Frequencies",
    "DISPLAY_TYPE": "spectrum",
    "SCALTYP": "log",
    "DICT_KEY": "power>spectral",
    "REFERENCE_POSITION": "1 AU",
    "SCALEMIN": 1.0E-24,
    "SCALEMAX": 1.0E-17,
    "VAR_TYPE": "data",
    "TARGET_CLASS": "planet",
}

DISTANCES = {
    'earth': 60*6400/1.5e8,  # = 0.00256
    'jupiter': 5.2,
    'saturn': 9.6,
    'uranus': 19.2,
    'neptune': 30.1,
}


def write_txt(file_txt=FILE_TXT):

    def export_table(fh, component, planet, title):
        f, s = data[component][0:2]
        fh.write(f"{title}\n")
        fh.write('-------------------------------------------------------------------------\n')
        for i in range(len(f)):
            ff = f[i]
            ss = s[i] * dist[planet] ** 2
            fh.write(f'{i:8d} {ff:12.7f}          {ss:14.7e}\n')
        fh.write('-------------------------------------------------------------------------\n')

    print(f'Creating {FILE_TXT.name}')

    data = load_data("sav", units=False)
    dist = DISTANCES
    with open(file_txt, 'w') as ft:
        ft.write('-------------------------------------------------------------------------\n')
        ft.write('   Index    Frequencies (MHz)   Flux densities (W/m^2/Hz normalized to 1 AU distance)\n')
        ft.write('-------------------------------------------------------------------------\n')
        export_table(ft, 'jup', 'jupiter', 'JUPITER average magnetospheric radio emissions, KOM to DAM wavelengths')
        export_table(ft, 'jupmax', 'jupiter', 'JUPITER peak magnetospheric radio emissions, KOM to DAM wavelengths')
        export_table(ft, 'nkom', 'jupiter', 'JUPITER average narrow band KOM emission (Io torus)')
        export_table(ft, 'nkommax', 'jupiter', 'JUPITER peak narrow band KOM emission (Io torus)')
        export_table(ft, 'akrn', 'earth', 'EARTH auroral kilometric radiation (Nightside)')
        export_table(ft, 'sat', 'saturn', 'SATURN auroral kilometric radiation')
        export_table(ft, 'ukr', 'uranus', 'URANUS auroral kilometric radiation')
        export_table(ft, 'nkr', 'neptune','NEPTUNE auroral kilometric radiation (Smooth)')
        export_table(ft, 'nepb', 'neptune', 'NEPTUNE auroral kilometric radiation (Bursts)')
        export_table(ft, 'sed', 'saturn', 'SATURN radio lightning')
        export_table(ft, 'ued', 'uranus', 'URANUS radio lightning')


def write_cdf(file_cdf=FILE_CDF):
    # write data into CDF file
    data = load_data("sav")

    if FILE_CDF.exists():
        FILE_CDF.unlink()

    pycdf.lib.set_backward(False)  # this is setting the CDF version to be used
    c = pycdf.CDF(str(FILE_CDF), '')
    print(f'Creating {FILE_CDF.name}')
    c.col_major(True)  # Column Major
    c.compress(pycdf.const.NO_COMPRESSION)  # No file level compression

    # Writing ISTP global attributes
    c.attrs["Project"] = [
        "PADC>Paris Astronomical Data Centre",
        "MASER>Measurement Analaysis and Simulation of Emissions in the Radio range",
    ]
    c.attrs['Discipline'] = "Planetary Physics>Waves"
    c.attrs['Data_type'] = ' '
    c.attrs['Descriptor'] = ' '
    c.attrs['Data_version'] = "1.0"
    c.attrs['Instrument_type'] = 'Radio and Plasma Waves (space)'
    c.attrs['Logical_file_id'] = 'PlanetaryFluxDensities'
    c.attrs['Logical_source'] = ' '
    c.attrs['Logical_source_description'] = 'Planetary Radio Emission Flux Densities'
    c.attrs['File_naming_convention'] = ' '
    c.attrs['Mission_group'] = ' '
    c.attrs['PI_name'] = "P. Zarka"
    c.attrs['PI_affiliation'] = 'LESIA, Observatoire de Paris-PSL, CNRS'
    c.attrs['Source_name'] = ' '
    c.attrs['TEXT'] = 'Solar, Planetary and Exoplanetary Radio Emission Spectra.'
    c.attrs['Generated_by'] = ['PADC', 'MASER']
    c.attrs['Generation_date'] = datetime.datetime.now().isoformat()
    c.attrs['LINK_TEXT'] = ["More details on ", "CDPP archive"]
    c.attrs['LINK_TITLE'] = ["LESIA Cassini Kronos webpage", "web site"]
    c.attrs['HTTP_LINK'] = ["http://www.lesia.obspm.fr/kronos", "https://cdpp-archive.cnes.fr"]
    c.attrs['MODS'] = " "
    c.attrs['Parents'] = 'all_spectra.sav'
    c.attrs['Rules_of_use'] = " "
    c.attrs['Skeleton_version'] = "1.0"
    c.attrs['Time_resolution'] = " "
    c.attrs['Acknowledgement'] = " "
    c.attrs['ADID_ref'] = " "
    c.attrs['Validate'] = " "

    for key, (freqs, spect, meta) in data.items():
        fkey, skey = [f'{t}{key}' for t in ['f', 's']]

        c.new(fkey, data=freqs, type=pycdf.const.CDF_REAL4,
            compress=pycdf.const.NO_COMPRESSION)
        c[fkey].attrs['UNITS'] = "MHz"
        c[fkey].attrs['CATDESC'] = "Observation Frequencies"
        c[fkey].attrs['DICT_KEY'] = "frequencies"
        c[fkey].attrs['FIELDNAM'] = fkey
        c[fkey].attrs.new('FILLVAL', data=CDF_REAL4_FILLVAL, type=pycdf.const.CDF_REAL4)
        c[fkey].attrs['SCALETYP'] = 'log'
        c[fkey].attrs['SCALEMIN'] = 1.0e-3
        c[fkey].attrs['SCALEMAX'] = 100.

        c.new(skey, data=spect, type=pycdf.const.CDF_REAL4,
            compress=pycdf.const.NO_COMPRESSION)
        c[skey].attrs['FIELDNAM'] = skey
        for k, v in DEFAULT_DATA_VAR_ATTRS.items():
            c[skey].attrs[k] = v
        c[skey].attrs['DEPEND_0'] = fkey
        c[skey].attrs.new('FILLVAL', data=CDF_REAL4_FILLVAL, type=pycdf.const.CDF_REAL4)
        c[skey].attrs['CATDESC'] = meta['title']
        c[skey].attrs['TARGET_NAME'] = meta.get('target_name', " ")
        c[skey].attrs['TARGET_REGION'] = meta.get('target_region', " ")
        c[skey].attrs['TARGET_CLASS'] = meta.get('target_class', " ")
        c[skey].attrs['FEATURE_NAME'] = meta.get('target_class', " ")
        c[skey].attrs['UCD'] = meta.get('ucd', " ")

    c.close()


def load_data(datasource="cdf", units=True):
    _datasource_dict = {
        "cdf": _load_from_cdf,
        "sav": _load_from_sav,
    }

    try:
        raw_data = _datasource_dict[datasource]()
    except KeyError as e:
        raise e

    meta = {
        "galdip": {
            "title": "Galactic Background",
            "target_class": "galaxy",
            "target_name": "Galaxy",
            "ucd": "phot.flux.density;stat.mean",
            "product_type": "spectrum",
        },
        "tn": {
            "title": "Quasi Thermal Noise",
            "target_name": "Local Medium",
            "ucd": "phot.flux.density;stat.mean",
            "product_type": "polygon",
        },
        "tnmoy": {
            "title": "Average Quasi Thermal Noise",
            "target_name": "Local Medium",
            "ucd": "phot.flux.density;stat.mean",
            "product_type": "spectrum",
        },
        "noise": {
            "title": "Background Noise",
            "ucd": "phot.flux.density;stat.mean",
            "product_type": "spectrum",
        },
        "sph": {
            "title": "Earth Lightning",
            "target_class": "planet",
            "target_name": "Earth",
            "target_region": "atmosphere",
            "feature_name": "lightning",
            "ucd": "phot.flux.density;stat.mean",
            "product_type": "polygon",
        },
        "quietsun": {
            "title": "Quiet Sun",
            "target_class": "star",
            "target_name": "Sun",
            "target_region": "surface",
            "feature_name": ["Solar Radio Emission", "Solar corona"],
            "ucd": "phot.flux.density;stat.mean",
            "product_type": "spectrum",
        },
        "distsun": {
            "title": "Disturbed Sun",
            "target_class": "star",
            "target_name": "Sun",
            "target_region": "surface",
            "feature_name": ["Solar Radio Emission", "Solar corona"],
            "ucd": "phot.flux.density;stat.max",
            "product_type": "spectrum",
        },
        "typiii": {
            "title": "Solar Type III Bursts (average)",
            "target_class": "star",
            "target_name": "Sun",
            "target_region": "heliosphere",
            "feature_name": ["Solar Radio Emission", "Type III"],
            "ucd": "phot.flux.density;stat.mean",
            "product_type": "spectrum",
        },
        "typiiimax": {
            "title": "Solar Type III Bursts (upper limit)",
            "target_class": "star",
            "target_name": "Sun",
            "target_region": "heliosphere",
            "feature_name": ["Solar Radio Emission", "Type III"],
            "ucd": "phot.flux.density;stat.max",
            "product_type": "spectrum",
        },
        "jup": {
            "title": "Jupiter QP/bKOM/HOM/DAM (average)",
            "target_class": "planet",
            "target_name": "Jupiter",
            "target_region": "magnetosphere",
            "feature_name": ["Planetary Radio Emission", "QP", "bKOM", "HOM", "DAM"],
            "ucd": "phot.flux.density;stat.mean",
            "product_type": "spectrum",
        },
        "nkom": {
            "title": "Jupiter nKOM (average)",
            "target_class": "planet",
            "target_name": "Jupiter",
            "target_region": "magnetosphere",
            "feature_name": ["Planetary Radio Emission", "nKOM"],
            "ucd": "phot.flux.density;stat.mean",
            "product_type": "spectrum",
        },
        "dim": {
            "title": "Jupiter Decimetric",
            "target_class": "planet",
            "target_name": "Jupiter",
            "target_region": "magnetosphere",
            "feature_name": ["Planetary Radio Emission", "DIM", "Radiation Belts"],
            "ucd": "phot.flux.density;stat.mean",
            "product_type": "spectrum",
        },
        "jupmax": {
            "title": "Jupiter QP/bKOM/HOM/DAM (upper limit)",
            "target_class": "planet",
            "target_name": "Jupiter",
            "target_region": "magnetosphere",
            "feature_name": ["Planetary Radio Emission", "QP", "bKOM", "HOM", "DAM"],
            "ucd": "phot.flux.density;stat.max",
            "product_type": "spectrum",
        },
        "nkommax": {
            "title": "Jupiter nKOM (upper limit)",
            "target_class": "planet",
            "target_name": "Jupiter",
            "target_region": "magnetosphere",
            "feature_name": ["Planetary Radio Emission", "nKOM"],
            "ucd": "phot.flux.density;stat.max",
            "product_type": "spectrum",
        },
        "sat": {
            "title": "Saturn Kilometric Radiation (SKR)",
            "target_class": "planet",
            "target_name": "Saturn",
            "target_region": "magnetosphere",
            "feature_name": ["Planetary Radio Emission", "SKR"],
            "ucd": "phot.flux.density;stat.mean",
            "product_type": "spectrum",
        },
        "ukr": {
            "title": "Uranus Kilometric Radiation (UKR)",
            "target_class": "planet",
            "target_name": "Uranus",
            "target_region": "magnetosphere",
            "feature_name": ["Planetary Radio Emission", "UKR"],
            "ucd": "phot.flux.density;stat.mean",
            "product_type": "spectrum",
        },
        "nkr": {
            "title": "Smooth Neptune Kilometric Radiation (sNKR)",
            "target_class": "planet",
            "target_name": "Neptune",
            "target_region": "magnetosphere",
            "feature_name": ["Planetary Radio Emission", "NKR"],
            "ucd": "phot.flux.density;stat.mean",
            "product_type": "spectrum",
        },
        "akrn": {
            "title": "Terrestrial Auroral Kilometric Radiation (AKR), night side",
            "target_class": "planet",
            "target_name": "Earth",
            "target_region": "magnetosphere",
            "feature_name": ["Planetary Radio Emission", "AKR"],
            "ucd": "phot.flux.density;stat.mean",
            "product_type": "spectrum",
        },
        "akrd": {
            "title": "Terrestrial Auroral Kilometric Radiation (AKR), day side",
            "target_class": "planet",
            "target_name": "Earth",
            "target_region": "magnetosphere",
            "feature_name": ["Planetary Radio Emission", "AKR"],
            "ucd": "phot.flux.density;stat.mean",
            "product_type": "spectrum",
        },
        "sed": {
            "title": "Saturn Electrostatic Discharges (SED)",
            "target_class": "planet",
            "target_name": "Saturn",
            "target_region": "atmosphere",
            "feature_name": ["Planetary Radio Emission", "Lightning", "SED"],
            "ucd": "phot.flux.density;stat.mean",
            "product_type": "spectrum",
        },
        "ued": {
            "title": "Uranus Electrostatic Discharges (UED)",
            "target_class": "planet",
            "target_name": "Uranus",
            "target_region": "atmosphere",
            "feature_name": ["Planetary Radio Emission", "Lightning", "UED"],
            "ucd": "phot.flux.density;stat.mean",
            "product_type": "spectrum",
        },
        "nepb": {
            "title": "Bursty Neptune Kilometric Radiation (bNKR)",
            "target_class": "planet",
            "target_name": "Neptune",
            "target_region": "magnetosphere",
            "feature_name": ["Planetary Radio Emission", "bNKR"],
            "ucd": "phot.flux.density;stat.mean",
            "product_type": "spectrum",
        },
        "exo": {
            "title": "Exoplanet Auroral Radio Emission (Jupiter like)",
            "target_class": "exoplanet",
            "target_region": "magnetosphere",
            "feature_name": ["Planetary Radio Emission"],
            "ucd": "phot.flux.density;stat.mean",
            "product_type": "spectrum",
        },
        "exolf": {
            "title": "Exoplanet Auroral Radio Emission (Saturn like)",
            "target_class": "exoplanet",
            "target_region": "magnetosphere",
            "feature_name": ["Planetary Radio Emission"],
            "ucd": "phot.flux.density;stat.mean",
            "product_type": "spectrum",
        },
    }

    if units:
        funit = Unit("MHz")
        sunit = Unit("W m-2 Hz-1")
    else:
        funit = 1
        sunit = 1

    data = {}
    for key in meta.keys():
        f, s = [raw_data[f'{t}{key}'][...] for t in ['f', 's']]
        if key in ['sed', 'ued']:
            # fixing spectral range (as in IDL routines)
            w = f > 3
            s = s[w]
            f = f[w]
        data[key] = (f * funit, s * sunit, meta[key])

    return data


def _load_from_sav(file_sav=FILE_SAV):
    from scipy.io import readsav
    return readsav(str(file_sav))


def _load_from_cdf(file_cdf=FILE_CDF):
    return pycdf.CDF(str(file_cdf))


if __name__ == "__main__":
    write_txt(FILE_TXT)
    write_cdf(FILE_CDF)
