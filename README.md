# Solar System Low Frequency Radio Spectra

## Abstract
This dataset contains the natural solar system low frequency radio emission spectra for the 
Sun, the Earth, Jupiter, Saturn, Uranus and Neptune, normalised to an observation distance 
of 1 Astronomical Unit.

## Citation
- **DOI:** https://doi.org/10.25935/yawf-af18
- **Publisher:** PADC/MASER
- **License:** [CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/legalcode) for data;
[MIT](https://opensource.org/licenses/MIT) for software.
- **Citation:** Zarka, P., B. Cecconi. (2022), Solar System Low Frequency Radio Spectra 
(Version 1.0) [Data set]. PADC/MASER. https://doi.org/10.25935/YAWF-AF18

## Data Files
- `all_spectra.cdf`: CDF version of dataset.
- `all_spectra.sav`: IDL saveset version of dataset.
- `PlanetaryFluxDensities.txt`: Plain ASCII file restricted to solar system planet spectra.

## Description
The dataset contains the spectral flux density distribution of low frequency planetary radio 
emission components;
- the Jovian magnetospheric radio emissions (with average and peak levels):
  - Quasi Periodic Bursts (QP),
  - Narrow-band Kilometric (nKOM),
  - Broad-band Kilometric (bKOM),
  - Hectometric (HOM),
  - Decametric (DAM),
  - Io-controlled Decametric (Io-DAM);
- the Jovian radiation belt spectrum (DIM - decimetric).
- the terrestrial Auroral Kilometric Radiation (AKR) from dayside and nightside; 
- the Saturn Kilometric Radiation (SKR) and Saturn Electrostatic Discharges (SED);
- the Uranus Kilometric Radiation (UKR) and Uranus Electrostatic Discharges (UED);
- the smooth Neptune Kilometric Radiation (NKR) and Neptune kilometric burst emission (NepB);
- the Solar average and peak emission spectrum (Solar type III bursts);
- the quiet and disturbed Sun spectra.

In addition, a script is provided to prepare a modeled galactic background spectral flux density.

## Scripts
- `convert.py`: read the `all_spectra.sav` (IDL Saveset) file to produce the `all_spectra.cdf` and 
`PlanetaryFluxDensities.txt
- `models.py`: set of functions in use in `display_data.ipynb`
- `idl/plot_spectra_color.pro`: IDL routine to reproduce the figures of Zarka et al. (2012) 
- `python/plot_spectra_color.ipynb`: iPython notebook to reproduce the figures of Zarka et al. (2012)
- `display_data.ipynb`: iPython notebook to load and display the dataset.
- `topcat_figure.sh`: TOPCAT stilts script to plot the data

## References
- Zarka, P., Bougeret, J.-L., Briand, C., Cecconi, B., Falcke, H., Girard, J., et al. (2012). Planetary 
and exoplanetary low frequency radio observations from the Moon. Planetary and Space Science, 74(1), 
156–166. https://doi.org/10.1016/j.pss.2012.08.004
