#!python3
import numpy as np
from astropy.units import Unit, Quantity
from astropy.constants import c, k_B


def galaxy(freqs: Quantity) -> Quantity:
    """Galactic background radiation as modeled by Dulk et al., Astron. Astrophys., 2001.

    :param freqs: an array of observation frequencies
    :type freqs: Quantity
    :returns: specific intensity in W/m2/Hz/sr
    """
    nu = freqs.to('MHz').value
    I_g = 2.48e-20
    I_eg = 1.06e-20

    tau_nu = 5.0 * nu**(-2.1)

    I_nu = I_g * nu**(-0.52)*(1-np.exp(-tau_nu))/tau_nu + I_eg * nu**(-0.8)*np.exp(-tau_nu)
    I_nu = I_nu * Unit('W m^-2 Hz^-1 sr^-1')
    return I_nu


def jansky_to_kelvin(frequencies: Quantity, spectrum: Quantity) -> Quantity:
    """Convert Jansky spectrum (W/m2/Hz) into brightness temperature (K)
    :param frequencies: frequency values
    :param spectrum: spectrum values
    :return: brightness temperature values
    """
    from astropy.constants import c, k_B
    if isinstance(spectrum, np.ma.masked_array):
        mask = spectrum.mask
    else:
        mask = None
    if not isinstance(spectrum, Quantity):
        spectrum = spectrum * Unit('W m^-2 Hz^-1')
    Bt = (spectrum*c**2 / (2*frequencies.to('Hz')**2) / k_B).to('K')
    if mask is None:
        return Bt
    else:
        return np.ma.masked_array(Bt, mask=mask)


# short dipole solid angle:
short_dipole = 8*np.pi / 3 * Unit('sr')
