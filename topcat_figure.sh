topcat -stilts plot2plane \
   xpix=1139 ypix=497 \
   xlog=true ylog=true xlabel='Freq. (MHz)' ylabel='Spectral Flux density @ 1 AU (W/m^2/Hz)' \
   xmin=0 xmax=106.8 ymin=3.0E-23 ymax=2.387E-18 \
   legend=true \
   in=data/all_spectra.cdf \
    shading=auto \
   layer_01=Mark \
      x_01=fjup \
      y_01=sjup*5.2*5.2 \
      size_01=2 color_01=black \
      leglabel_01=Jupiter \
   layer_02=Mark \
      x_02=fjupmax \
      y_02=sjupmax*5.2*5.2 \
      color_02=black \
   layer_03=Mark \
      x_03=fnkom \
      y_03=snkom*5.2*5.2 \
      size_03=2 color_03=black \
   layer_04=Mark \
      x_04=fnkommax \
      y_04=snkommax*5.2*5.2 \
      color_04=black \
   layer_05=Mark \
      x_05=fakrn \
      y_05=sakrn*0.00256*0.00256 \
      size_05=2 color_05=green \
      leglabel_05=Earth \
   layer_06=Mark \
      x_06=fsat \
      y_06=ssat*9.6*9.6 \
      size_06=2 \
      leglabel_06=Saturn \
   layer_07=Mark \
      x_07=fukr \
      y_07=sukr*19.2*19.2 \
      size_07=2 color_07=magenta \
      leglabel_07=Uranus \
   layer_08=Mark \
      x_08=fnkr \
      y_08=snkr*30.1*30.1 \
      size_08=2 color_08=magenta \
      leglabel_08=Neptune \
   layer_09=Mark \
      x_09=fnepb \
      y_09=snepb*30.1*30.1 \
      color_09=magenta \
   layer_10=Mark \
      x_10=fsed \
      y_10=ssed*9.6*9.6 \
   layer_11=Mark \
      x_11=fued \
      y_11=sued*19.2*19.2 \
      color_11=magenta \
   legseq=_01,_05,_06,_07,_08