;------------------------------------------
; plot_spectres_color.pro
;------------------------------------------

set_plot,'ps'
device,file='plot_spectres_color.ps'
device,/color
device,/landscape
!p.font=0
loadct,13

jy=1.e-26
djup=5.2 & dsat=9.6 & dura=19.2 & dnep=30.1
restore,'../data/all_spectra.sav'

w=where(fsed ge 3) & fsed=fsed(w) & ssed=ssed(w)
w=where(fued ge 3) & fued=fued(w) & sued=sued(w)

;									color
;	galdip		tnmoy		noise		rec noise	-
;	tn		sph						105
;	quietSun	distSun		typIII		typIIImax	215
;	jup		nkom		dim				245
;	jupmax		nkommax						245
;	sat		ukr		nkr				85 / 155 / 125
;	sed		ued						85 / 155
;	akrN		akrD						50
;	exo		exoLF						245 / 85

;window,0,xs=700,ys=700
!p.charsize=1.3
!p.thick=5.

; plot 1 - planetes only @ 1 AU

plot_oo,fjup,sjup*(djup^2),xra=[0.01,50],/xsty,yra=[2e-23,5e-19],/ysty, $
  xtit='Frequency (MHz)',ytit='Flux Density at 1 AU (Wm!U-2!NHz!U-1!N)'
oplot,fjup,sjup*(djup^2),color=245,thick=9
oplot,fakrN,sakrN*(60.*6400/1.5e8)^2,color=50,thick=9.
oplot,fsat,ssat*(dsat^2),color=85,thick=9.
oplot,fukr,sukr*(dura^2),color=155,thick=9.
oplot,fnkr,snkr*(dnep^2),color=125,thick=9.

; plot 2

plot_oo,fgaldip,sgaldip,xra=[0.01,100],/xsty,yra=[1e-23,1e-14],/ysty, $
  xtit='Frequency (MHz)',ytit='Flux Density at Moon Orbit (Wm!U-2!NHz!U-1!N)'
polyfill,ftn,stn,/line_fill,orient=45,color=105
polyfill,fsph,ssph,/line_fill,orient=45,color=105
oplot,fgaldip,sgaldip
oplot,ftnmoy,stnmoy,line=2
oplot,[0.01,100],[0,0]+2.6e-21,line=1	; ~ receiver noise 7 nV/sqr(Hz) with L=7m
oplot,fquietSun,squietSun,color=215
oplot,fdistSun,sdistSun,color=215
oplot,ftypIII,stypIII,color=215
oplot,ftypIIImax,stypIIImax,color=215,line=2
oplot,fakrN,sakrN,color=50,line=2
oplot,fakrD,sakrD,color=50
oplot,[0.030,0.030],[1.e-31,1.],line=1
oplot,[0.30,0.30],[1.e-31,1.],line=1
oplot,[10,10],[1.e-31,1.],line=1

; plot 3

plot_oo,fgaldip,sgaldip,xra=[0.01,100],/xsty,yra=[1e-26,1e-14],/ysty, $
  xtit='Frequency (MHz)',ytit='Flux Density at Moon Orbit (Wm!U-2!NHz!U-1!N)'
polyfill,ftn,stn,/line_fill,orient=45,color=105
polyfill,fsph,ssph,/line_fill,orient=45,color=105
oplot,fgaldip,sgaldip
oplot,ftnmoy,stnmoy,line=2
oplot,[0.01,100],[0,0]+2.6e-21,line=1	; ~ receiver noise 7 nV/sqr(Hz) with L=7m
oplot,fquietSun,squietSun,color=215
oplot,fdistSun,sdistSun,color=215
oplot,ftypIII,stypIII,color=215
oplot,ftypIIImax,stypIIImax,color=215,line=2
oplot,fakrN,sakrN,color=50,line=2
oplot,fakrD,sakrD,color=50
oplot,[0.030,0.030],[1.e-31,1.],line=1
oplot,[10,10],[1.e-31,1.],line=1
oplot,fjup,sjup,color=245,thick=9
oplot,fnkom,snkom,color=245,thick=9
oplot,fdim,sdim,color=245,thick=9
oplot,fjupmax,sjupmax,color=245,line=2,thick=9
oplot,fnkommax,snkommax,color=245,line=2,thick=9

; plot 4

plot_oo,fgaldip,sgaldip,xra=[0.01,100],/xsty,yra=[1e-26,1e-14],/ysty, $
  xtit='Frequency (MHz)',ytit='Flux Density at Moon Orbit (Wm!U-2!NHz!U-1!N)'
polyfill,ftn,stn,/line_fill,orient=45,color=105
polyfill,fsph,ssph,/line_fill,orient=45,color=105
oplot,fgaldip,sgaldip
oplot,ftnmoy,stnmoy,line=2
oplot,[0.01,100],[0,0]+2.6e-21,line=1	; ~ receiver noise 7 nV/sqr(Hz) with L=7m
oplot,fquietSun,squietSun,color=215
oplot,fdistSun,sdistSun,color=215
oplot,ftypIII,stypIII,color=215
oplot,ftypIIImax,stypIIImax,color=215,line=2
oplot,fakrN,sakrN,color=50,line=2
oplot,fakrD,sakrD,color=50
oplot,[0.030,0.030],[1.e-31,1.],line=1
oplot,[10,10],[1.e-31,1.],line=1
oplot,fjup,sjup,color=245,thick=9.
oplot,fnkom,snkom,color=245,thick=9.
oplot,fdim,sdim,color=245,thick=9.
oplot,fjupmax,sjupmax,color=245,line=2,thick=9.
oplot,fnkommax,snkommax,color=245,line=2,thick=9.
oplot,fsat,ssat,color=85,thick=9.
oplot,fukr,sukr,color=155,thick=9.
oplot,fnkr,snkr,color=125,thick=9.

; plot 5

plot_oo,fgaldip,sgaldip,xra=[0.01,100],/xsty,yra=[1e-26,1e-14],/ysty, $
  xtit='Frequency (MHz)',ytit='Flux Density at Moon Orbit (Wm!U-2!NHz!U-1!N)'
polyfill,ftn,stn,/line_fill,orient=45,color=105
polyfill,fsph,ssph,/line_fill,orient=45,color=105
oplot,fgaldip,sgaldip
oplot,ftnmoy,stnmoy,line=2
oplot,[0.01,100],[0,0]+2.6e-21,line=1	; ~ receiver noise 7 nV/sqr(Hz) with L=7m
oplot,fquietSun,squietSun,color=215
oplot,fdistSun,sdistSun,color=215
oplot,ftypIII,stypIII,color=215
oplot,ftypIIImax,stypIIImax,color=215,line=2
oplot,fakrN,sakrN,color=50,line=2
oplot,fakrD,sakrD,color=50
oplot,[0.030,0.030],[1.e-31,1.],line=1
oplot,[0.30,0.30],[1.e-31,1.],line=1
oplot,[10,10],[1.e-31,1.],line=1
oplot,fjup,sjup,color=245,thick=9.
oplot,fnkom,snkom,color=245,thick=9.
oplot,fdim,sdim,color=245,thick=9.
oplot,fjupmax,sjupmax,color=245,line=2,thick=9.
oplot,fnkommax,snkommax,color=245,line=2,thick=9.
oplot,fsat,ssat,color=85,thick=9.
oplot,fukr,sukr,color=155,thick=9.
oplot,fnkr,snkr,color=125,thick=9.
oplot,fnepb,snepb,color=125,thick=9.,line=2
oplot,fsed,ssed,color=85,thick=9.,line=2
oplot,fued,sued,color=155,thick=9.,line=2
oplot,[5,5],[1.e-25,5.e-23],line=1,color=30,thick=9.

; plot 6

plot_oo,[10,10],[1.e-31,1.],line=1,xra=[0.01,100],/xsty,yra=[1e-26,1e-18],/ysty, $
  xtit='Frequency (MHz)',ytit='Flux Density at Moon Orbit (Wm!U-2!NHz!U-1!N)'
oplot,fjup,sjup,color=245,thick=9.
oplot,fnkom,snkom,color=245,thick=9.
oplot,fdim,sdim,color=245,thick=9.
oplot,fjupmax,sjupmax,color=245,line=2,thick=9.
oplot,fnkommax,snkommax,color=245,line=2,thick=9.
oplot,fsat,ssat,color=85,thick=9.
oplot,fukr,sukr,color=155,thick=9.
oplot,fnkr,snkr,color=125,thick=9.
oplot,fnepb,snepb,color=125,thick=9.,line=2
oplot,fsed,ssed,color=85,thick=9.,line=2
oplot,fued,sued,color=155,thick=9.,line=2
oplot,[5,5],[1.e-25,5.e-23],line=1,color=30,thick=9.

; plot 7

plot_oo,fgaldip,sgaldip,xra=[1,100],/xsty,yra=[1e-26,1e-14],/ysty, $
  xtit='Frequency (MHz)',ytit='Flux Density at Moon Orbit (Wm!U-2!NHz!U-1!N)'
polyfill,fsph,ssph,/line_fill,orient=45,color=105
oplot,fgaldip,sgaldip
oplot,fquietSun,squietSun,color=215
oplot,fdistSun,sdistSun,color=215
oplot,ftypIII,stypIII,color=215
oplot,ftypIIImax,stypIIImax,color=215,line=2
oplot,fakrN,sakrN,color=50,line=2
oplot,fakrD,sakrD,color=50
oplot,[0.030,0.030],[1.e-31,1.],line=1
oplot,[10,10],[1.e-31,1.],line=1
oplot,fjup,sjup,color=245,thick=9.
oplot,fnkom,snkom,color=245,thick=9.
oplot,fdim,sdim,color=245,thick=9.
oplot,fjupmax,sjupmax,color=245,line=2,thick=9.
oplot,fnkommax,snkommax,color=245,line=2,thick=9.
oplot,fsat,ssat,color=85,thick=9.
oplot,fukr,sukr,color=155,thick=9.
oplot,fnkr,snkr,color=125,thick=9.
oplot,fsed,ssed,color=85,thick=9.,line=2
oplot,fued,sued,color=155,thick=9.,line=2
oplot,[5,5],[1.e-25,5.e-23],line=1,color=30,thick=9.
oplot,fgaldip,sgaldip/1.e2,thick=3.
oplot,fgaldip,sgaldip/1.e4,thick=3.
oplot,fgaldip,sgaldip/1.e6,thick=3.

; plot 8

plot_oo,fnoise,snoise,xra=[0.03,100],/xsty,yra=[1e-26,1e-18],/ysty, $
  xtit='Frequency (MHz)',ytit='Flux Density at Moon Orbit (Wm!U-2!NHz!U-1!N)',line=2
oplot,[0.30,0.30],[1.e-31,1.],line=1
oplot,[10,10],[1.e-31,1.],line=1
oplot,fjup,sjup,color=245,thick=9.
oplot,fnkom,snkom,color=245,thick=9.
oplot,fdim,sdim,color=245,thick=9.
oplot,fjupmax,sjupmax,color=245,line=2,thick=9.
oplot,fnkommax,snkommax,color=245,line=2,thick=9.
oplot,fsat,ssat,color=85,thick=9.
oplot,fukr,sukr,color=155,thick=9.
oplot,fnkr,snkr,color=125,thick=9.
oplot,fnepb,snepb,color=125,thick=9.,line=2
oplot,fsed,ssed,color=85,thick=9.,line=2
oplot,fued,sued,color=155,thick=9.,line=2
oplot,[5,5],[1.e-25,5.e-23],line=1,color=30,thick=9.
oplot,fnoise,snoise/1.e2,thick=3.
oplot,fnoise,snoise/1.e4,thick=3.
oplot,fnoise,snoise/1.e6,thick=3.

; plot 9

plot_oo,fgaldip,sgaldip,xra=[1,100],/xsty,yra=[1e-31,1e-14],/ysty, $
  xtit='Frequency (MHz)',ytit='Flux Density at Moon Orbit (Wm!U-2!NHz!U-1!N)',line=2
polyfill,fsph,ssph,/line_fill,orient=45,color=105
oplot,fgaldip,sgaldip,line=2
oplot,fquietSun,squietSun,color=215
oplot,fdistSun,sdistSun,color=215
oplot,ftypIII,stypIII,color=215
oplot,ftypIIImax,stypIIImax,color=215,line=2
oplot,fakrN,sakrN,color=50,line=2
oplot,fakrD,sakrD,color=50
oplot,[10,10],[1.e-31,1.],line=1
oplot,fgaldip,sgaldip/1.e2,thick=3.
oplot,fgaldip,sgaldip/1.e4,thick=3.
oplot,fgaldip,sgaldip/1.e6,thick=3.
oplot,fgaldip,sgaldip/1.e8,thick=3.
oplot,fgaldip,sgaldip/1.e10,thick=3.
oplot,fexo,sexo,color=245,line=2,thick=9.
oplot,fexo,sexo*1.e1,color=245,thick=9.
oplot,fexo,sexo*1.e3,color=245,thick=9.
oplot,fexo,sexo*1.e5,color=245,thick=9.

; plot 10

plot_oo,fgaldip,sgaldip,xra=[0.01,100],/xsty,yra=[1e-31,1e-14],/ysty, $
  xtit='Frequency (MHz)',ytit='Flux Density at Moon Orbit (Wm!U-2!NHz!U-1!N)'
polyfill,ftn,stn,/line_fill,orient=45,color=105
polyfill,fsph,ssph,/line_fill,orient=45,color=105
oplot,fgaldip,sgaldip
oplot,fquietSun,squietSun,color=215
oplot,fdistSun,sdistSun,color=215
oplot,ftypIII,stypIII,color=215
oplot,ftypIIImax,stypIIImax,color=215,line=2
oplot,fakrN,sakrN,color=50,line=2
oplot,fakrD,sakrD,color=50
oplot,[0.030,0.030],[1.e-31,1.],line=1
oplot,[10,10],[1.e-31,1.],line=1
oplot,fexo,sexo,color=245,line=2,thick=9.
oplot,fexo,sexo*1.e1,color=245,thick=9.
oplot,fexo,sexo*1.e3,color=245,thick=9.
oplot,fexo,sexo*1.e5,color=245,thick=9.

; plot 11

plot_oo,fnoise,snoise,xra=[0.03,50],/xsty,yra=[1e-31,1e-18],/ysty, $
  xtit='Frequency (MHz)',ytit='Flux Density at Moon Orbit (Wm!U-2!NHz!U-1!N)',line=2
oplot,[0.30,0.30],[1.e-31,1.],line=1
oplot,[10,10],[1.e-31,1.],line=1
oplot,fnoise,snoise/1.e2,thick=3.
oplot,fnoise,snoise/1.e4,thick=3.
oplot,fnoise,snoise/1.e6,thick=3.
oplot,fnoise,snoise/1.e8,thick=3.
oplot,fnoise,snoise/1.e10,thick=3.
oplot,fexo,sexo,color=245,line=2,thick=9.
oplot,fexo,sexo*1.e1,color=245,thick=9.
oplot,fexo,sexo*1.e3,color=245,thick=9.
oplot,fexo,sexo*1.e5,color=245,thick=9.
oplot,fexoLF,sexoLF,color=85,line=2,thick=9.
oplot,fexoLF,sexoLF*1.e1,color=85,thick=9.
oplot,fexoLF,sexoLF*1.e3,color=85,thick=9.
oplot,fexoLF,sexoLF*1.e5,color=85,thick=9.

; plot 12

plot_oo,fgaldip,sgaldip,xra=[0.05,2.5],/xsty,yra=[1e-31,1e-19],/ysty, $
  xtit='Frequency (MHz)',ytit='Flux Density at Moon Orbit (Wm!U-2!NHz!U-1!N)',line=2
oplot,[0.01,0.7],[1.25e-17,1.25e-17/(70^2)],line=2	; ~ typical TN
oplot,[0.01,100],[0,0]+2.6e-21,line=1	; ~ receiver noise 7 nV/sqr(Hz) with L=7m
oplot,fgaldip,sgaldip/1.e2,thick=3.
oplot,fgaldip,sgaldip/1.e4,thick=3.
oplot,fgaldip,sgaldip/1.e6,thick=3.
oplot,fgaldip,sgaldip/1.e8,thick=3.
oplot,fgaldip,sgaldip/1.e10,thick=3.
oplot,[0.01,0.7],[1.25e-17,1.25e-17/(70^2)]/1.e2,thick=3.
oplot,[0.01,0.7],[1.25e-17,1.25e-17/(70^2)]/1.e4,thick=3.
oplot,[0.01,0.7],[1.25e-17,1.25e-17/(70^2)]/1.e6,thick=3.
oplot,[0.01,0.7],[1.25e-17,1.25e-17/(70^2)]/1.e8,thick=3.
oplot,[0.01,0.7],[1.25e-17,1.25e-17/(70^2)]/1.e10,thick=3.
oplot,fexoLF,sexoLF,color=85,line=2,thick=9.
oplot,fexoLF,sexoLF*1.e1,color=85,thick=9.
oplot,fexoLF,sexoLF*1.e3,color=85,thick=9.
oplot,fexoLF,sexoLF*1.e5,color=85,thick=9.

device,/close
exit_ps
ps_pdf,/rem

openw,u,'PlanetaryFluxDensities.txt',/get_lun
printf,u,'-------------------------------------------------------------------------'
printf,u,'   Index    Frequencies (MHz)   Flux densities (W/m^2/Hz normalized to 1 AU distance)'

printf,u,'-------------------------------------------------------------------------'
printf,u,'JUPITER average magnetospheric radio emissions, KOM to DAM wavelengths'
printf,u,'-------------------------------------------------------------------------'
f=fjup & s=sjup*(djup^2)
for i=0,n_elements(f)-1 do printf,u,i,f(i),'        ',s(i)

printf,u,'-------------------------------------------------------------------------'
printf,u,'JUPITER peak magnetospheric radio emissions, KOM to DAM wavelengths'
printf,u,'-------------------------------------------------------------------------'
f=fjupmax & s=sjupmax*(djup^2)
for i=0,n_elements(f)-1 do printf,u,i,f(i),'        ',s(i)

printf,u,'-------------------------------------------------------------------------'
printf,u,'JUPITER average narrow band KOM emission (Io torus)'
printf,u,'-------------------------------------------------------------------------'
f=fnkom & s=snkom*(djup^2)
for i=0,n_elements(f)-1 do printf,u,i,f(i),'        ',s(i)

printf,u,'-------------------------------------------------------------------------'
printf,u,'JUPITER peak narrow band KOM emission (Io torus)'
printf,u,'-------------------------------------------------------------------------'
f=fnkommax & s=snkommax*(djup^2)
for i=0,n_elements(f)-1 do printf,u,i,f(i),'        ',s(i)

printf,u,'-------------------------------------------------------------------------'
printf,u,'EARTH auroral kilometric radiation (Nightside)'
printf,u,'-------------------------------------------------------------------------'
f=fakrN & s=sakrN*(60.*6400/1.5e8)^2
for i=0,n_elements(f)-1 do printf,u,i,f(i),'        ',s(i)

printf,u,'-------------------------------------------------------------------------'
printf,u,'SATURN auroral kilometric radiation'
printf,u,'-------------------------------------------------------------------------'
f=fsat & s=ssat*(dsat^2)
for i=0,n_elements(f)-1 do printf,u,i,f(i),'        ',s(i)

printf,u,'-------------------------------------------------------------------------'
printf,u,'URANUS auroral kilometric radiation'
printf,u,'-------------------------------------------------------------------------'
f=fukr & s=sukr*(dura^2)
for i=0,n_elements(f)-1 do printf,u,i,f(i),'        ',s(i)

printf,u,'-------------------------------------------------------------------------'
printf,u,'NEPTUNE auroral kilometric radiation (Smooth)'
printf,u,'-------------------------------------------------------------------------'
f=fnkr & s=snkr*(dnep^2)
for i=0,n_elements(f)-1 do printf,u,i,f(i),'        ',s(i)

printf,u,'-------------------------------------------------------------------------'
printf,u,'NEPTUNE auroral kilometric radiation (Bursts)'
printf,u,'-------------------------------------------------------------------------'
f=fnepb & s=snepb*(dnep^2)
for i=0,n_elements(f)-1 do printf,u,i,f(i),'        ',s(i)

printf,u,'-------------------------------------------------------------------------'
printf,u,'SATURN radio lightning'
printf,u,'-------------------------------------------------------------------------'
f=fsed & s=ssed*(dsat^2)
for i=0,n_elements(f)-1 do printf,u,i,f(i),'        ',s(i)

printf,u,'-------------------------------------------------------------------------'
printf,u,'URANUS radio lightning'
printf,u,'-------------------------------------------------------------------------'
f=fued & s=sued*(dura^2)
for i=0,n_elements(f)-1 do printf,u,i,f(i),'        ',s(i)

printf,u,'-------------------------------------------------------------------------'
close,u & free_lun,u

end